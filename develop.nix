{ pkgs ? import ./nixpkgs.nix }: with pkgs; let

  ghcCharged =  haskellPackages.ghcWithHoogle (p: with p; [
                  haskell-language-server
                  ghcid
                  threadscope
                ]);
  ghcid-bin = haskellPackages.ghcid.bin;

  ghcid-test = let
    ghcid = "${ghcid-bin}/bin/ghcid";
    out = "$out/bin/ghcid-test";
  in runCommand "ghcid-test" { buildInputs = [ makeWrapper ]; } ''
    makeWrapper ${ghcid} ${out} --add-flags "--command='cabal repl test:test-testing' --test 'Main.main'"
  '';

in mkShell {
  buildInputs =  haskellPackages.testing.env.nativeBuildInputs ++
                 [ ghcCharged
                   ghcid-bin
                   ghcid-test
                   cabal-install
                   selenium-server-standalone
                   geckodriver
                 ];

}
