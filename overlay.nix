final: prev: with final; {

  haskellPackages = prev.haskellPackages.override (old: {
    overrides = lib.composeManyExtensions [
                  (old.overrides or (_: _: {}))
                  (haskell.lib.packageSourceOverrides { testing = ./.; })
                ];
  });

  testing = haskell.lib.justStaticExecutables haskellPackages.testing;

  ghcWithTesting = haskellPackages.ghcWithPackages (p: [ p.testing ]);

  ghcWithTestingAndPackages = select :
    haskellPackages.ghcWithPackages (p: ([ p.testing ] ++ select p));


  jupyterlab = mkJupyterlab {
    haskellKernelName = "testing";
    haskellPackages = p: with p;
      [ # add haskell pacakges if necessary
        testing
        hvega
        ihaskell-hvega
      ];
    pythonKernelName = "testing";
    pythonPackages = p: with p;
      [ # add python pacakges if necessary
        scipy
        numpy
        tensorflow-bin
        matplotlib
        scikit-learn
        pandas
        lightgbm
      ];
  };

}
